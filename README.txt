@Author Ahmed
@date: 1/08/2018
This is a weather application. It shows the temperature at 
- The current location
- A suburb
- Temperature next 7 days.

The app is written using Xamarin.Form. You can use this app for free of charge. Any modifcation is allowed.

Images taken from https://www.istockphoto.com/au/vector/weather-news-reporter-gm488573961-39485794?irgwc=1&esource=AFF_IS_IR_SP_FreeImages_246195&asid=FreeImages&cid=IS
