﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WeatherApp
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async void ViewCurrentLocation(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new CurrentLocationPage());
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
            }
        }

        async void ViewAtLocation(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new TempAtLocationPage());
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
            }
        }

        async void ViewTemperatureNext7Days(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new TemperatureNext7Days());
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
            }
        }

        async void SignupClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new SignupPage());
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
            }
        }
    }
}
